#!/usr/bin/env python
# encoding: utf-8
# Created Time: 2017年08月22日 星期二 21时36分48秒

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def main():
    game_csv = '/home/huizhu/bitbucket/test_data/game_logs.csv'
    banking_csv = '/home/huizhu/bitbucket/test_data/banking.csv'
    alogps_csv = '/home/huizhu/bitbucket/mol_data/test_schema/alogps.csv'
    delaney_csv = '/home/huizhu/bitbucket/mol_data/test_schema/delaney.csv'

    bk = pd.read_csv(delaney_csv)
    print(bk.shape)
    print(bk.info(memory_usage='deep'))

    for dtype in ['float', 'int', 'object']:
        selected_dtype = bk.select_dtypes(include=[dtype])
        usage = selected_dtype.memory_usage(index=False, deep=True)
        print(dtype, selected_dtype.shape)
        print(usage)



if __name__ == '__main__':
    main()
