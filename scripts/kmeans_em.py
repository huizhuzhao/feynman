#!/usr/bin/env python
# encoding: utf-8
# author: huizhu
# created time: 2017年08月08日 星期二 13时40分58秒

import time
import numpy as np
import argparse
import matplotlib.pyplot as plt
from Erwin.utils import numpy_utils as utils

def prepare_dataset(n_samples, k):
    sigma = 0.5
    x_list = []
    y_list = []
    for ii in range(k):
        centers = np.asarray([ii, ii])
        x = np.random.randn(n_samples, 2) * sigma + centers
        y = np.ones((n_samples, 1)) * ii
        x_list.append(x)
        y_list.append(y)

    X = np.concatenate(x_list, axis=0)
    Y = np.concatenate(y_list, axis=0)

    return X, Y


class KMeans(object):
    def __init__(self, X, k):
        assert len(X.shape) == 2
        self.X = X
        self.k = k

        n_samples, dim = self.X.shape
        self.mu = np.random.randn(self.k, dim)
        self.labels = np.random.randint(0, k, size=(n_samples,))

    def e_step(self):
        dist = utils.l2_distance(self.X, self.mu)
        self.labels = np.argmin(dist, axis=1)


    def m_step(self):
        for label in range(self.k):
            index = np.where(self.labels == label)
            samples = self.X[index]
            self.mu[label] = np.mean(samples, axis=0)



def main():
    parser = argparse.ArgumentParser(description='Kmeans algorithm for split gaussian dataset')

    parser.add_argument('--n_samples', type=int, default=1000, 
                        help='num of samples for each categories')
    parser.add_argument('--k', type=int, default=2, 
                        help='num of categories')

    args = parser.parse_args()

    X, Y = prepare_dataset(args.n_samples, args.k)

    plt.scatter(X[:, 0], X[:, 1], c=Y)
    plt.savefig('../test_true.png')

    kmeans = KMeans(X, args.k)


    for ii in range(10):
        plt.scatter(kmeans.X[:, 0], kmeans.X[:, 1], c=kmeans.labels)
        plt.scatter(kmeans.mu[:, 0], kmeans.mu[:, 1], marker='o', s=100, c='r')
        print(kmeans.mu.shape)
        plt.savefig('../test_{0}.png'.format(ii))
        plt.show()
        kmeans.e_step()
        kmeans.m_step()




if __name__ == '__main__':
    main()
