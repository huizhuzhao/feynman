#!/usr/bin/env python
# encoding: utf-8
# author: huizhu
# created time: 2017年07月18日 星期二 09时30分38秒

import numpy as np
<<<<<<< HEAD
import json

import char_loader

from keras import backend as K
from keras.models import Sequential
from keras import layers
from keras.layers import Dense, Embedding, LSTM
from keras.callbacks import ModelCheckpoint, CSVLogger


SEQ_LEN = char_loader.DIGITS * 2 + 1
VOCA_SIZE = len(char_loader.VOCA)
EMBED_DIM = 100
HIDDEN_DIM = 150
LOGS_DIR = './logs'


def get_callbacks(log_dir):
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    log_file = os.path.join(log_dir, 'logger.csv')
    model_name = os.path.join(log_dir, 'rnn')
    model_name = model_name + '.{epoch:02d}--{val_acc:.2f}.hdf5'

    logger = CSVLogger(log_file, append=False)
    checkpoint = ModelCheckpoint(model_name, monitor='val_acc',
            verbose=1, save_best_only=True)
    callbacks = [checkpoint, logger]
    return callbacks


def dump_config(config, log_dir):
    path = os.path.join(log_dir, 'config.json')
    with open(path, 'w') as f_w:
        json.dump(config, f_w)
=======
import tensorflow as tf
from Erwin.models.tf_utils import tf_core
>>>>>>> 175ad130b10c88fdc792fce4183fb93899559969

def custom_metrics(y_true, y_pred):
    y_true = tf.argmax(y_true, axis=-1)
    y_pred = tf.argmax(y_pred, axis=-1)
    acc = tf.equal(y_true, y_pred)
    acc = tf.cast(tf.reduce_all(acc, axis=1), tf.float32)
    acc = tf.reduce_mean(acc)

    return acc

<<<<<<< HEAD
def get_model1():
    model = Sequential()
    model.add(Embedding(VOCA_SIZE, EMBED_DIM, input_length=SEQ_LEN))
    model.add(LSTM(HIDDEN_DIM, return_sequences=True))
    model.add(LSTM(HIDDEN_DIM, return_sequences=False))
    model.add(layers.RepeatVector(char_loader.DIGITS + 1))
    model.add(LSTM(HIDDEN_DIM, return_sequences=True))
    model.add(layers.TimeDistributed(Dense(VOCA_SIZE, activation='softmax'))) # share same weights

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['acc'])
    return model

def get_model2():
    config_path = os.path.join(LOGS_DIR, 'config.json')
    hdf5_path = os.path.join(LOGS_DIR, 'VGG.99--0.62.hdf5')
    config = json.load(open(config_path, 'r'))
    model = Sequential.from_config(config)
    model.load_weights(hdf5_path)
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['acc'])
    return model


def main():
    train_data, valid_data = char_loader.gen_questions_answers()
    train_X = char_loader.featurize(train_data[0])
    train_y = char_loader.featurize(train_data[1])
    train_y = np.eye(VOCA_SIZE)[train_y]
    train_X2 = train_X[:, ::-1]
    combined_X = np.concatenate([train_X, train_X2], axis=0)
    combined_y = np.concatenate([train_y, train_y], axis=0)

    valid_X = char_loader.featurize(valid_data[0])
    valid_y = char_loader.featurize(valid_data[1])
    valid_y = np.eye(VOCA_SIZE)[valid_y]
    print('train_X.shape: {0}'.format(train_X.shape))
    print('train_y.shape: {0}'.format(train_y.shape))
    print('combined_X/y.shape: {0}/{1}'.format(combined_X.shape, combined_y.shape))

    model = get_model2()
    
    dump_config(model.get_config(), LOGS_DIR)
    callbacks = get_callbacks(LOGS_DIR)
    model.fit(combined_X, combined_y, batch_size=64, epochs=100, validation_data=(valid_X, valid_y), callbacks=callbacks)
    val_res = model.evaluate(valid_X, valid_y)
    tra_res = model.evaluate(train_X, train_y)
    print(model.input_shape)
    print(model.output_shape)
    print(val_res)
    print(tra_res)
=======
def numpy_acc(y_true, y_pred):
    y_true = np.argmax(y_true, axis=-1)
    y_pred = np.argmax(y_pred, axis=-1)
    acc = np.all(y_true == y_pred, axis=1)
    acc = np.mean(acc)
    return acc


def main():
    y_true = tf_core.placeholder(shape=(None, 3, 12))
    y_pred = tf_core.placeholder(shape=(None, 3, 12))
    acc = custom_metrics(y_true, y_pred)

    y_true_numpy = np.random.uniform(0, 1, size=(1000, 3, 12))
    y_pred_numpy = np.random.uniform(0, 1, size=(1000, 3, 12))

    sess = tf.Session()
    acc_tf = sess.run(acc, feed_dict={y_true: y_true_numpy, y_pred: y_pred_numpy})


    acc_numpy = numpy_acc(y_true_numpy, y_pred_numpy)
>>>>>>> 175ad130b10c88fdc792fce4183fb93899559969

    print(acc)
    print(acc_numpy)
    print(acc_tf)


if __name__ == '__main__':
    main()

