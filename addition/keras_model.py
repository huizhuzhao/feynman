#!/usr/bin/env python
# encoding: utf-8
# Created Time: Sun 16 Jul 2017 10:36:26 PM CST

import os
import numpy as np
import json

import char_loader

import tensorflow as tf
from keras import backend as K
from keras.models import Sequential
from keras import layers
from keras.layers import Dense, Embedding, LSTM
from keras.callbacks import ModelCheckpoint, CSVLogger


SEQ_LEN = char_loader.DIGITS * 2 + 1
VOCA_SIZE = len(char_loader.VOCA)
EMBED_DIM = 100
HIDDEN_DIM = 150
LOGS_DIR = './logs_{0}'.format(char_loader.DIGITS)


def get_callbacks(log_dir):
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    log_file = os.path.join(log_dir, 'logger.csv')
    model_name = os.path.join(log_dir, 'rnn')
    model_name = model_name + '.{epoch:02d}--{val_acc:.2f}.hdf5'

    logger = CSVLogger(log_file, append=True)
    checkpoint = ModelCheckpoint(model_name, monitor='val_acc',
            verbose=1, save_best_only=True)
    callbacks = [checkpoint, logger]
    return callbacks


def dump_config(config, log_dir):
    path = os.path.join(log_dir, 'config.json')
    with open(path, 'w') as f_w:
        json.dump(config, f_w)

def custom_metrics(y_true, y_pred):
    y_true_reduced = K.argmax(y_true, axis=-1)
    y_pred_reduced = K.argmax(y_pred, axis=-1)
    acc = K.equal(y_true_reduced, y_pred_reduced)
    acc = K.cast(K.all(acc, axis=1), dtype=tf.float32)
    acc = K.mean(acc)

    return acc


def model_1():
    model = Sequential()
    model.add(Embedding(VOCA_SIZE, EMBED_DIM, input_length=SEQ_LEN))
    model.add(LSTM(HIDDEN_DIM, return_sequences=True))
    model.add(LSTM(HIDDEN_DIM, return_sequences=False))
    model.add(layers.RepeatVector(char_loader.DIGITS + 1))
    model.add(LSTM(HIDDEN_DIM, return_sequences=True))
    model.add(layers.TimeDistributed(Dense(VOCA_SIZE, activation='softmax'))) # share same weights
    return model

def model_2():
    config_path = os.path.join(LOGS_DIR, 'config.json')
    hdf5_path = os.path.join(LOGS_DIR, 'rnn.00--0.84.hdf5')
    with open(config_path, 'r') as f_r:
        config = json.load(f_r)

    model = Sequential().from_config(config)
    model.load_weights(hdf5_path)
    return model



def main():
    train_data, valid_data = char_loader.gen_questions_answers()
    train_X = char_loader.featurize(train_data[0])
    train_y = char_loader.featurize(train_data[1])
    train_y = np.eye(VOCA_SIZE)[train_y]

    valid_X = char_loader.featurize(valid_data[0])
    valid_y = char_loader.featurize(valid_data[1])
    valid_y = np.eye(VOCA_SIZE)[valid_y]
    print('train_X.shape: {0}'.format(train_X.shape))
    print('train_y.shape: {0}'.format(train_y.shape))

    model = model_2()
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['acc', custom_metrics])

    """ 
    callbacks = get_callbacks(LOGS_DIR)
    dump_config(model.get_config(), LOGS_DIR)
    model.fit(train_X, train_y, batch_size=64, epochs=500, validation_data=(valid_X, valid_y), callbacks=callbacks)
    """ 
    train_res = model.evaluate(train_X, train_y)
    valid_res = model.evaluate(valid_X, valid_y)
    print(train_res)
    print(valid_res)
    print(model.input_shape)
    print(model.output_shape)



if __name__ == '__main__':
    main()
