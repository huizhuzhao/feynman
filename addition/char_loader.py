#!/usr/bin/env python
# encoding: utf-8
# Created Time: Fri 16 Jun 2017 09:12:05 PM CST

import numpy as np

DIGITS = 3
PAD = '#'
VOCA = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+'] + [PAD]
VOCA_TO_IDX = dict(zip(VOCA, range(len(VOCA))))
dtype_INT = np.int32
dtype_FLOAT = np.float32

    
def featurize(string_list):
    assert isinstance(string_list, list)

    def _featurize_one_string(string):
        idx = [VOCA_TO_IDX[x] for x in list(string)]
        idx = np.asarray(idx, dtype=dtype_INT)

        return idx

    indices = []
    for s in string_list:
        idx = _featurize_one_string(s)
        indices.append(idx)

    indices = np.asarray(indices)
    return indices




def gen_questions_answers():
    """
    return list of questions and answers
    >>> train_q[:3], train_a[:3] 
    >>> (['##0+##0', '##0+##1', '##0+##2'], ['###0', '###1', '###2'])
    """
    def _pad_strings(list_int, max_len):
        list_int = [str(x) for x in list_int]
        padded = [(max_len - len(x)) * PAD + x for x in list_int]

        return padded

    def _foo(indices):
        x_triu = x[indices]
        y_triu = y[indices]
        z_triu = z[indices]

        x_list = _pad_strings(x_triu, DIGITS) 
        y_list = _pad_strings(y_triu, DIGITS) 
        z_list = _pad_strings(z_triu, DIGITS + 1) 

        questions = [a + '+' + b for a, b in zip(x_list, y_list)]
        answers = z_list[:]

        return questions, answers

    max_num = 10 ** DIGITS
    x = np.arange(max_num).reshape((-1, 1))
    y = np.arange(max_num).reshape((1, -1))

    x = np.tile(x, (1, max_num))
    y = np.tile(y, (max_num, 1))
    z = (x + y)

    train_idx = np.triu_indices(max_num, 0)
    valid_idx = np.tril_indices(max_num, -1)

    train_q, train_a = _foo(train_idx)
    valid_q, valid_a = _foo(valid_idx)

    return (train_q, train_a), (valid_q, valid_a)

