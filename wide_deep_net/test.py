#!/usr/bin/env python
# encoding: utf-8
# Created Time: 2017年07月31日 星期一 22时06分33秒

import os
import tempfile
import tensorflow as tf
import pandas as pd
import urllib

# Define the column names for the data sets.
COLUMNS = ["age", "workclass", "fnlwgt", "education", "education_num",
  "marital_status", "occupation", "relationship", "race", "gender",
  "capital_gain", "capital_loss", "hours_per_week", "native_country", "income_bracket"]
LABEL_COLUMN = 'label'
CATEGORICAL_COLUMNS = ["workclass", "education", "marital_status", "occupation",
                       "relationship", "race", "gender", "native_country"]
CONTINUOUS_COLUMNS = ["age", "education_num", "capital_gain", "capital_loss",
                      "hours_per_week"]
USER_PATH = os.path.expanduser('~')

def main():
    gender = tf.contrib.layers.sparse_column_with_keys(column_name='gender', keys=['Female', 'Male'])
    train_file = os.path.join(USER_PATH, 'bitbucket/test_data/wide_deep_net/adult.data.txt')
    test_file = os.path.join(USER_PATH, 'bitbucket/test_data/wide_deep_net/adult.test.txt')
    print(os.path.exists(train_file), os.path.exists(test_file))
    df_train = pd.read_csv(train_file)
    df_test = pd.read_csv(test_file)
    print(df_train.shape, df_test.shape)
    print(df_train.head())

    

    """
    m = tf.contrib.learn.DNNLinearCombinedClassifier(
        model_dir=model_dir,
        linear_feature_columns=wide_columns,
        dnn_feature_columns=deep_columns,
        dnn_hidden_units=[100, 50])
    """



if __name__ == '__main__':
    main()
